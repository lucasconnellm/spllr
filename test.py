# coding: utf-8
import eng_to_ipa as ipa
from nltk.corpus import cmudict
fish = ipa.convert('fish')
tough = ipa.convert('tough', retrieve_all=True)
women = ipa.convert('women', retrieve_all=True)
nation = ipa.convert('nation', retrieve_all=True)
print('fish\'s phonetic spelling would be: {}'.format(fish))
print('tough, women, nation would be (in order): {}, {}, {}'.format(tough[0], women[0], nation[0]))
print('so one can see, the phonemes exist in tough, women, and nation to sound out fish')
print('but how can we tell which syllables match to which phonemes?')
# Other cool ipa stuff
ipa.isin_cmu('testing if all of these words are in the cmu dictionary')
ipa.isin_cmu('asdfasdf')
# we can do the above analysis with cmu purely if we want, though I haven't looked into it as much

